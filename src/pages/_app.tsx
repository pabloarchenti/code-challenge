import { FC } from 'react';
import { AppProps } from 'next/app';

export const MyApp: FC<AppProps> = ({ Component, pageProps }) => (
  <Component {...pageProps} />
);

export default MyApp;

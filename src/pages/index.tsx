import Maze from '@components/maze';
import Layout from '@components/layout';
import { useState } from 'react';

const grid = [
  [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
  [1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1],
  [1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1],
  [1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1],
  [1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1],
  [1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1],
  [1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1],
  [1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1],
  [1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1],
  [1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
];
const entry: [number, number] = [0, 1];

const postMovements = async (movements) => {
  try {
    const url = `${process.env.NEXT_PUBLIC_API_BASEURL}/5df38f523100006d00b58560`;
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify({ movements }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    if (response.ok) {
      return true;
    }
  } catch (err) {} // eslint-disable-line
  return false;
};

export const IndexPage = () => {
  const [postResult, setPostResult] = useState(null);

  const onFinish = async (movements) => {
    setPostResult(await postMovements(movements));
    setTimeout(() => {
      setPostResult(null);
    }, 2500);
  };

  return (
    <Layout title="CookUnity Challenge">
      <Maze grid={grid} entry={entry} onFinish={onFinish} />
      {postResult === true && (
        <div className="notify success">Movements submitted!</div>
      )}
      {postResult === false && (
        <div className="notify error">Movements submission failed!</div>
      )}
      <style jsx>{`
        .notify {
          position: absolute;
          z-index: 101;
          top: 0;
          left: 0;
          right: 0;
          background: #e0e0e0;
          text-align: center;
          font-weight: bold;
          line-height: 2.5;
          overflow: hidden;
          box-shadow: 0 0 5px black;
        }
        .success {
          color: green;
        }
        .error {
          color: red;
        }
      `}</style>
    </Layout>
  );
};

export default IndexPage;

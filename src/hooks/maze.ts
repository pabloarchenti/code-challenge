import { useReducer, useEffect } from 'react';
import { Maze, WallFollowerSolver, Position, Grid } from '@lib/maze';

interface Options {
  speed?: number;
  solverName?: string;
  onFinish?: (movements: Position[]) => void;
}

const defaultSpeed = 150;
const defaultSolverName = 'wall-follower';
let interval;
let running = false;

const getMovements = ({ grid, entry, solverName }) => {
  const maze = new Maze(grid, entry);
  let solver = null;
  if (solverName === 'wall-follower') {
    solver = new WallFollowerSolver(maze);
  }
  if (!solver) {
    throw new Error('Maze solver not found');
  }
  return solver.solve().getMovements();
};

const init = ({ grid, entry, solverName }) => {
  return {
    movements: getMovements({ grid, entry, solverName }),
    index: 0,
  };
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'reset': {
      return init(action.payload);
    }
    case 'move': {
      return {
        ...state,
        index:
          state.index < state.movements.length - 1
            ? state.index + 1
            : state.index,
      };
    }
    default:
      throw new Error('useMaze: unknown action');
  }
};

export const useMaze = (
  grid: Grid,
  entry: Position,
  {
    speed = defaultSpeed,
    solverName = defaultSolverName,
    onFinish = () => {}, // eslint-disable-line
  }: Options,
) => {
  const [{ index, movements }, dispatch] = useReducer(
    reducer,
    { grid, entry, solverName },
    init,
  );

  const stop = () => {
    running = false;
    clearInterval(interval);
  };

  const reset = () => {
    stop();
    dispatch({
      type: 'reset',
      payload: {
        grid,
        entry,
        solverName,
      },
    });
  };

  const start = () => {
    reset();
    running = true;
    interval = setInterval(() => {
      dispatch({ type: 'move' });
    }, speed);
  };

  useEffect(() => {
    if (index === movements.length - 1 && running) {
      stop();
      onFinish(movements);
    }
  });

  return {
    position: movements[index],
    movement: index + 1,
    start,
  };
};

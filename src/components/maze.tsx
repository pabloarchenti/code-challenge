import { FC, useEffect } from 'react';
import Board from './board';
import { Position, Grid } from '@lib/maze';
import { useMaze } from '@hooks/maze';

const MazeComponent: FC<{
  grid: Grid;
  entry: Position;
  onFinish?: (movements: Position[]) => void;
}> = ({ grid, entry, onFinish }) => {
  const { position, movement, start } = useMaze(grid, entry, {
    onFinish,
  });

  useEffect(() => {
    start();
  }, []);

  return (
    <>
      <div className="header">
        <div className="title">CookUnity</div>
        <div className="moves">Moves: {movement}</div>
      </div>
      <Board grid={grid} position={position} />
      <button
        onClick={() => {
          start();
        }}
        className="button"
      >
        Rerun
      </button>
      <style jsx>
        {`
          .header {
            display: flex;
            margin: 15px 0px 0px;
          }
          .title {
            font-size: 25px;
            font-weight: bold;
          }
          .moves {
            text-align: right;
            width: 100%;
          }
          .button {
            float: right;
            margin: 10px 10px;
            font-weight: bold;
            padding: 10px;
          }
        `}
      </style>
    </>
  );
};

export default MazeComponent;

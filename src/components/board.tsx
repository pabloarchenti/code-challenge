import type { FC } from 'react';
import Square from './square';

const Board: FC<{ grid: number[][]; position: [number, number] }> = ({
  grid,
  position,
}) => {
  return (
    <div className="board">
      {grid.map((row, rowIndex) => {
        return row.map((cell, colIndex) => {
          return (
            <Square
              close={cell ? true : false}
              occupied={rowIndex === position[0] && colIndex === position[1]}
              key={`${rowIndex}${colIndex}`}
            />
          );
        });
      })}
      <style jsx>{`
        .board {
          min-width: 300px;
          min-height: 300px;
          height: 80vh;
          display: grid;
          grid-template-columns: repeat(${grid.length}, minmax(0, 1fr));
          grid-gap: 1px;
        }
      `}</style>
    </div>
  );
};

export default Board;

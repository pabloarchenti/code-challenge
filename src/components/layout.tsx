import Head from 'next/head';
import { FC } from 'react';

export const Layout: FC<{ title: string }> = ({ title, children }) => (
  <>
    <Head>
      <title>{title}</title>
      <link rel="icon" type="image/png" sizes="16x16" href="favicon.png" />
    </Head>
    <main>
      {children}
      <style jsx>{`
        main {
          margin: 0 auto;
          max-width: 1024px;
        }
      `}</style>
    </main>
  </>
);

export default Layout;

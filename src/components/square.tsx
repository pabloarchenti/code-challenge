import type { FC } from 'react';
import classNames from 'classnames';

const Square: FC<{ close: boolean; occupied: boolean }> = ({
  close,
  occupied = false,
}) => (
  <div className={classNames('square', { occupied })}>
    <style jsx>{`
      .square {
        background-color: ${close ? 'black' : 'white'};
      }
      .occupied {
        background-image: url('avatar.png');
        background-repeat: no-repeat;
        background-position: center center;
        background-size: cover;
      }
    `}</style>
  </div>
);

export default Square;

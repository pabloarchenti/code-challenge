/* eslint-disable no-fallthrough */
export type Grid = number[][];
export type Position = [number, number];

export enum Movement {
  RIGHT,
  FORWARD,
  LEFT,
  BACKWARD,
}

export enum State {
  OPEN,
  CLOSE,
  OUT,
}

enum Direction {
  NORTH,
  EAST,
  SOUTH,
  WEST,
}

export class Maze {
  private readonly grid: Grid;
  private readonly entry: Position;
  private readonly exit: Position;
  private position: Position;
  private direction: Direction;

  constructor(grid: Grid, entry: Position) {
    this.grid = grid;
    this.entry = entry;
    this.reset();
    this.validate();
    this.calculateStartDirection();
  }

  reset() {
    this.position = this.entry;
  }

  getPosition() {
    return this.position;
  }

  getSize() {
    return this.grid.length * this.grid[0].length;
  }

  move(movement: Movement) {
    const newDirection = this.calculateDirection(movement);
    const newPosition = this.calculatePosition(newDirection);
    const newPositionState = this.evaluatePosition(newPosition);

    if (newPositionState === State.OPEN) {
      this.direction = newDirection;
      this.position = newPosition;
    }

    return newPositionState;
  }

  private validate() {
    if (this.grid.length < 4 || this.grid[0].length < 4) {
      throw new Error('maze is too small');
    }
    for (let i = 0; i < this.grid.length; i++) {
      for (let j = 0; j < this.grid[i].length; j++) {
        if (this.grid[i][j] !== 0 && this.grid[i][j] !== 1) {
          throw new Error(
            "Input grid has to be set with 1's (wall) and 0's (open) only",
          );
        }
      }
    }
  }

  private evaluatePosition(position): State {
    const cell = this.grid[position[0]] && this.grid[position[0]][position[1]];
    if (cell === 1) return State.CLOSE;
    if (cell === 0) return State.OPEN;

    return State.OUT;
  }

  private calculatePosition(direction: Direction): Position {
    switch (direction) {
      case Direction.NORTH:
        return [this.position[0] - 1, this.position[1]];
      case Direction.EAST:
        return [this.position[0], this.position[1] + 1];
      case Direction.SOUTH:
        return [this.position[0] + 1, this.position[1]];
      case Direction.WEST:
        return [this.position[0], this.position[1] - 1];
    }
  }

  private calculateDirection(movement: Movement) {
    switch (movement) {
      case Movement.RIGHT:
        switch (this.direction) {
          case Direction.NORTH:
            return Direction.EAST;
          case Direction.EAST:
            return Direction.SOUTH;
          case Direction.SOUTH:
            return Direction.WEST;
          case Direction.WEST:
            return Direction.NORTH;
        }
      case Movement.LEFT:
        switch (this.direction) {
          case Direction.NORTH:
            return Direction.WEST;
          case Direction.EAST:
            return Direction.NORTH;
          case Direction.SOUTH:
            return Direction.EAST;
          case Direction.WEST:
            return Direction.SOUTH;
        }
      case Movement.BACKWARD:
        switch (this.direction) {
          case Direction.NORTH:
            return Direction.SOUTH;
          case Direction.EAST:
            return Direction.WEST;
          case Direction.SOUTH:
            return Direction.NORTH;
          case Direction.WEST:
            return Direction.EAST;
        }
      case Movement.FORWARD:
        return this.direction;
    }
  }

  private calculateStartDirection(): Direction {
    let newPosition = this.calculatePosition(Direction.WEST);

    if (this.evaluatePosition(newPosition) === State.OUT) {
      this.direction = Direction.EAST;
      return;
    }

    newPosition = this.calculatePosition(Direction.EAST);
    if (this.evaluatePosition(newPosition) === State.OUT) {
      this.direction = Direction.WEST;
      return;
    }

    newPosition = this.calculatePosition(Direction.NORTH);
    if (this.evaluatePosition(newPosition) === State.OUT) {
      this.direction = Direction.SOUTH;
      return;
    }

    newPosition = this.calculatePosition(Direction.SOUTH);
    if (this.evaluatePosition(newPosition) === State.OUT) {
      this.direction = Direction.NORTH;
      return;
    }
  }
}

import { Maze, Grid, Position, Movement, State } from './maze';

const grid = [
  [1, 1, 1, 0, 1],
  [1, 0, 0, 0, 1],
  [1, 1, 0, 1, 1],
  [1, 0, 0, 0, 1],
  [1, 1, 1, 0, 1],
];
const entry: Position = [0, 3];

describe('Maze', () => {
  describe('Constructor', () => {
    it('grid should be big enough', () => {
      const grid: Grid = [
        [1, 0, 1],
        [1, 0, 1],
      ];
      expect(() => {
        new Maze(grid, [0, 1]);
      }).toThrow();
    });

    it("grid should containt only 0's and 1's", () => {
      const grid: Grid = [
        [1, 0, 2],
        [1, 0, 1],
      ];
      expect(() => {
        new Maze(grid, [0, 1]);
      }).toThrow();
    });
  });

  it('reset() should make current position equal to the entry', () => {
    const maze = new Maze(grid, entry);
    maze.move(Movement.FORWARD);
    maze.move(Movement.RIGHT);
    maze.reset();
    expect(maze.getPosition()).toEqual(entry);
  });

  it('getSize() should return total number of cells', () => {
    const maze = new Maze(grid, entry);
    expect(maze.getSize()).toBe(25);
  });

  describe('move()', () => {
    let maze = null;

    beforeEach(() => {
      maze = new Maze(grid, [0, 3]);
    });

    it('move forward: passage open', () => {
      const state = maze.move(Movement.FORWARD);
      expect(state).toBe(State.OPEN);
      expect(maze.getPosition()).toEqual([1, 3]);
    });

    it('move right: passage close', () => {
      const prevPosition = maze.getPosition();
      const state = maze.move(Movement.RIGHT);
      expect(state).toBe(State.CLOSE);
      expect(maze.getPosition()).toEqual(prevPosition);
    });

    it('move left: passage close', () => {
      const prevPosition = maze.getPosition();
      const state = maze.move(Movement.LEFT);
      expect(state).toBe(State.CLOSE);
      expect(maze.getPosition()).toEqual(prevPosition);
    });

    it('move backward: out of maze', () => {
      const prevPosition = maze.getPosition();
      const state = maze.move(Movement.BACKWARD);
      expect(state).toBe(State.OUT);
      expect(maze.getPosition()).toEqual(prevPosition);
    });
  });
});

import { Movement, State } from './maze';
import { MazeSolver } from './maze.solver';

export class WallFollowerSolver extends MazeSolver {
  solve() {
    this.reset();
    let finished = false;
    const mazeSize = this.maze.getSize();
    this.movements.push(this.maze.getPosition());

    while (!finished) {
      const nextMovements: Movement[] = [
        Movement.RIGHT,
        Movement.FORWARD,
        Movement.LEFT,
        Movement.BACKWARD,
      ];

      if (this.getMovements().length > mazeSize) {
        throw new Error('Maze cannot be solved');
      }

      for (const nextMovement of nextMovements) {
        const state = this.maze.move(nextMovement);
        if (state == State.OPEN) {
          const newPosition = this.maze.getPosition();
          this.movements.push(newPosition);
          break;
        } else if (state == State.OUT) {
          finished = true;
          break;
        }
      }
    }

    return this;
  }
}

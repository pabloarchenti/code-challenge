import { Maze, Position } from './maze';

export abstract class MazeSolver {
  protected readonly maze: Maze;
  protected movements: Position[];

  constructor(maze: Maze) {
    this.maze = maze;
    this.reset();
  }

  protected reset() {
    this.maze.reset();
    this.movements = [];
  }

  getMovements() {
    return this.movements;
  }

  abstract solve(): void;
}

import { WallFollowerSolver } from './wall-follower.solver';
import { Maze, Position } from './maze';

describe('WallFollowerSolver', () => {
  it('should solve a perfect maze with only one solution', () => {
    const grid = [
      [1, 1, 1, 0, 1],
      [1, 0, 0, 0, 1],
      [1, 1, 0, 1, 1],
      [1, 0, 0, 0, 1],
      [1, 1, 1, 0, 1],
    ];
    const entry: Position = [0, 3];
    const solution = [
      [0, 3],
      [1, 3],
      [1, 2],
      [1, 1],
      [1, 2],
      [2, 2],
      [3, 2],
      [3, 1],
      [3, 2],
      [3, 3],
      [4, 3],
    ];
    const maze = new Maze(grid, entry);
    const solver = new WallFollowerSolver(maze);
    solver.solve();
    expect(solver.solve()).toBe(solver);
    expect(solver.getMovements()).toEqual(solution);
  });

  it('should solve a perfect maze with more than one solution', () => {
    const grid = [
      [1, 1, 1, 0, 1],
      [1, 0, 0, 0, 1],
      [1, 1, 0, 1, 1],
      [1, 0, 0, 0, 1],
      [1, 0, 1, 0, 1],
    ];
    const entry: Position = [0, 3];
    const solution = [
      [0, 3],
      [1, 3],
      [1, 2],
      [1, 1],
      [1, 2],
      [2, 2],
      [3, 2],
      [3, 1],
      [4, 1],
    ];
    const maze = new Maze(grid, entry);
    const solver = new WallFollowerSolver(maze);
    solver.solve();
    expect(solver.getMovements()).toEqual(solution);
  });

  it("should return to entry when there's no exit", () => {
    const grid = [
      [1, 1, 0, 1],
      [1, 0, 0, 1],
      [1, 0, 0, 1],
      [1, 1, 1, 1],
    ];
    const entry: Position = [0, 2];
    const solution = [
      [0, 2],
      [1, 2],
      [1, 1],
      [2, 1],
      [2, 2],
      [1, 2],
      [0, 2],
    ];
    const maze = new Maze(grid, entry);
    const solver = new WallFollowerSolver(maze);
    solver.solve();
    expect(solver.getMovements()).toEqual(solution);
  });

  it('should throw an error when maze cannot be solved', () => {
    const grid = [
      [1, 1, 1, 1],
      [1, 0, 0, 1],
      [1, 0, 0, 1],
      [1, 1, 1, 1],
    ];
    const entry: Position = [1, 2];
    const maze = new Maze(grid, entry);
    const solver = new WallFollowerSolver(maze);
    expect(() => {
      solver.solve();
    }).toThrow();
  });
});

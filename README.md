# Code challenge for [Cookunity](https://www.cookunity.com/)

View [PDF with specifications](challenge-specifications.pdf)

## Resolution

### Assumptions / Constraints

- Focus on solving perfect mazes from inside or above.
- Although it was not explicitly required by the challenge I developed the solution to be easily extensible.
- Selected algorithm: Wall follower.

### Missing

- Testing of React components.

### Alternatives

- Use React Context API to store number of movements.

### Running example

![Running example](running-example.gif 'Running example')

## Install App

### With Docker compose

```bash
$ docker-compose up --build
```

To change default port run it with "PORT" environment variable.

### With Docker

```bash
$ docker build -t cookunity/archenti .
$ docker run -ti --rm -p 3000:3000 cookunity/archenti
```

> Using Node image based on Alpine so it weighs less than 100MB.

### Manually

```bash
$ npm install
$ npm build
$ npm start [-- -p PORT]
```

## Run App

Access http://localhost:[PORT]/

_Default PORT is 3000._

## Tests

```bash
$ npm run test
```

## Main dependencies

- NodeJS v12 (LTS)
- NextJS v9.4
- React v16.13
- Typescript v3.9
- Jest v26
